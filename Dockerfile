FROM python:3.5-alpine3.8
# TODO Set metadata for who maintains this image
LABEL "maintainer"="David Desjardins"

COPY * /app/

RUN pip3 install Flask  && apk add curl

EXPOSE 8080

#TODO Set default values for env variables
ENV DISPLAY_COLOR="green"
ENV DISPLAY_FONT="arial"
ENV ENVIRONMENT="Prod"

#TODO *bonus* add a health check that tells docker the app is running properly
HEALTHCHECK --interval=30s --timeout=3s \
  CMD curl -f http://localhost:8080 || exit 1

# TODO have the app run as a non-root user
USER 1001

CMD python /app/app.py

